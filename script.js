// TODO: check calls to connect and disconnect

const tolerance = 0.1;
const conDist = 0.2;

class Message {
	constructor(type, ...args) {
		this.type = type;
		this.args = args;
	}
}

class PlayerConnection {
	constructor(index, xPos, yPos, angles, connections) {
		this.index = index;
		this.xPos = xPos;
		this.yPos = yPos;
		this.angles = angles;
		this.connections = { };
		for (let c of connections) this.connections[c] = true;
		this.disconnectReceived = false;
		this.disconnectReady = false;
	}
	
	send(msg, index) {
		players[this.index].message(msg, index);
	}
}

class Player {
	constructor(index) {
		this.index = index;
		
		this.xPos = Math.random() * 2 - 1;
		this.yPos = Math.random() * 2 - 1;
		
		this.xVel = 0;
		this.yVel = 0;
		
		this.xAcc = 0;
		this.yAcc = 0;
		
		this.angles = [ ];
		this.connections = { };
	}
	
	message(msg, sIndex) {
		let sender = this.connections[sIndex];
		
		const connect = (i1, i2) => {
			if (i1 in this.connections) this.connections[i1].connections[i2] = true;
			if (i2 in this.connections) this.connections[i2].connections[i1] = true;
		}
		
		const disconnect = (i1, i2) => {
			if (i1 in this.connections) delete this.connections[i1].connections[i2];
			if (i2 in this.connections) delete this.connections[i2].connections[i1];
		}
		
		const checkConnections = (c) => {
			// TODO: only connect best connections (rather than all connections)
			// Sort by distance to fix connection problem?
			// Maybe connect one at a time until all connection holes are patched
			function checkAngles(c, o) {
				let angle = Math.atan2(o.yPos - c.yPos, o.xPos - c.xPos) / (2 * Math.PI);
				let a1 = angle - 0.25, a2 = angle + 0.25;
				if (a1 < 0) a1 += 1;
				if (a2 < 0) a2 += 1;
				for (let [r1, r2] of c.angles) {
					if (r2 > a1 && r1 < a2 || a2 < a1 && (r2 > a1 || r1 < a2)) return true;
				}
			}
			
			for (let o of Object.values(this.connections)) {
				if (c != o && !c.connections[o.index]) {
					if ((o.xPos - c.xPos) ** 2 + (o.yPos - c.yPos) ** 2 < conDist ** 2
						|| checkAngles(c, o)
						|| checkAngles(o, c)) {
						c.send(new Message('connect', o.index, o.xPos, o.yPos, o.angles, o.connections), this.index);
						o.send(new Message('connect', c.index, c.xPos, c.yPos, c.angles, c.connections), this.index);
						connect(c.index, o.index);
					}
				}
			}
		}
		
		switch (msg.type) {
			case 'connect': { // TODO: Check if connection needed?
				let [index, xPos, yPos, angles, connections] = msg.args;
				let c = new PlayerConnection(index, xPos, yPos, angles, Object.keys(connections));
				this.connect(c);
				
				checkConnections(c);
				break;
			}
			case 'disconnect': {
				sender.disconnectReceived = true;
				if (sender.disconnectReady) sender.send(new Message('disconnect2'), this.index);
				break;
			}
			case 'disconnect2': {
				if (sender.disconnectReady) {
					this.disconnect(sIndex);
					this.reconnect();
				}
				break;
			}
			case 'nodisconnect': {
				sender.disconnectReceived = false;
				break;
			}
			case 'connected': {
				let [oIndex] = msg.args;
				connect(oIndex, sIndex);
				break;
			}
			case 'disconnected': {
				let [oIndex] = msg.args;
				disconnect(oIndex, sIndex);
				break;
			}
			case 'move': {
				let [xPos, yPos, angles] = msg.args;
				sender.xPos = xPos;
				sender.yPos = yPos;
				sender.angles = angles;
				
				checkConnections(sender);
				break;
			}
		}
	}
	
	connect(pCon) {
		for (let c of Object.values(this.connections)) c.send(new Message('connected', pCon.index), this.index);
		
		this.connections[pCon.index] = pCon;
	}
	
	disconnect(pIndex, otherDisconnected) {
		delete this.connections[pIndex];
		if (!otherDisconnected) players[pIndex].disconnect(this.index, true);
		
		for (let c of Object.values(this.connections)) c.send(new Message('disconnected', pIndex), this.index);
	}
	
	move() {
		this.xAcc += (Math.random() * 2 - 1) / 100;
		this.yAcc += (Math.random() * 2 - 1) / 100;
		
		if (this.xVel > 0.7) this.xAcc -= (this.xVel - 0.7) * 0.1;
		else if (this.xVel < -0.7) this.xAcc -= (this.xVel + 0.7) * 0.1;
		if (this.yVel > 0.7) this.yAcc -= (this.yVel - 0.7) * 0.1;
		else if (this.yVel < -0.7) this.yAcc -= (this.yVel + 0.7) * 0.1;
		
		this.xVel += this.xAcc / 200;
		this.yVel += this.yAcc / 200;
		
		if (this.xPos > 0.7) this.xVel -= (this.xPos - 0.7) * 0.1;
		else if (this.xPos < -0.7) this.xVel -= (this.xPos + 0.7) * 0.1;
		if (this.yPos > 0.7) this.yVel -= (this.yPos - 0.7) * 0.1;
		else if (this.yPos < -0.7) this.yVel -= (this.yPos + 0.7) * 0.1;
		
		this.xPos += this.xVel / 200;
		this.yPos += this.yVel / 200;
		
		this.reconnect();
	}
	
	reconnect() {
		this.reconnected = false;
		function overlap(list, angle, delta) {
			let a1 = angle - delta, a2 = angle + delta;
			if (a1 < 0) {
				a1 += 1;
				a2 += 1;
			}
			let i = 1;
			while (i < list.length && list[i][0] < a1) i++;
			if (i < list.length && list[i][0] == a1) {
				list[i][1]++;
				if (list[i][1] == 0) list.splice(i, 1);
			}
			else list.splice(i, 0, [ a1, 1 ]);
			i += 1;
			if (a2 > 1) {
				list[0]++;
				i = 1;
				a2 -= 1;
			}
			while (i < list.length && list[i][0] < a2) i++;
			if (i < list.length && list[i][0] == a2) {
				list[i][1]--;
				if (list[i][1] == 0) list.splice(i, 1);
			}
			else list.splice(i, 0, [ a2, -1 ]);
		}
		
		let toConnect = [ 0 ], toDisconnect = [ 0 ];
		for (let c of Object.values(this.connections)) {
			let xDist = c.xPos - this.xPos;
			let yDist = c.yPos - this.yPos;
			let a = Math.atan2(yDist, xDist) / (2 * Math.PI);
			let d = Math.sqrt(xDist ** 2 + yDist ** 2);
			overlap(toConnect, a, 0.25);
			if (d > tolerance) overlap(toDisconnect, a, Math.acos(tolerance / d) / (2 * Math.PI));
		}
		
		this.angles = [ ];
		let current = toConnect[0];
		let start = 0;
		for (let i = 1; i < toConnect.length; i++) {
			if (current == 0) this.angles.push([ start, toConnect[i][0] ]);
			current += toConnect[i][1];
			if (current == 0) start = toConnect[i][0];
		}
		if (current == 0) this.angles.push([ start, 1 ]);
		
		for (let c of Object.values(this.connections)) {
			c.send(new Message('move', this.xPos, this.yPos, this.angles), this.index);
			
			let xDist = c.xPos - this.xPos;
			let yDist = c.yPos - this.yPos;
			if (xDist ** 2 + yDist ** 2 > (conDist + tolerance) ** 2) {
				let needed = false;
				if (toDisconnect.length > 1) {
					let angle = Math.atan2(yDist, xDist) / (2 * Math.PI);
					let a1 = angle - 0.25, a2 = angle + 0.25;
					if (a1 < 0) {
						a1 += 1;
						a2 += 1;
					}
					let i = 1;
					let current = toDisconnect[0];
					while (i < toDisconnect.length && toDisconnect[i][0] <= a1) {
						current += toDisconnect[i][1];
						i++;
					}
					// TODO: refactor loop
					if (i >= toDisconnect.length) {
						i = 1;
						a2 -= 1;
					}
					while (toDisconnect[i][0] < a2) {
						if (current <= 1) {
							needed = true;
							break;
						}
						current += toDisconnect[i][1];
						i++;
						if (i >= toDisconnect.length) {
							i = 1;
							a2 -= 1;
						}
					}
					if (current <= 1) needed = true;
				} else needed = toDisconnect[0] <= 1;
				
				if (!needed) {
					if (!c.disconnectReady) {
						c.disconnectReady = true;
						c.send(new Message('disconnect'), this.index);
						if (this.reconnected) return;
					}
				} else if (c.disconnectReady) {
					c.send(new Message('nodisconnect'), this.index);
					c.disconnectReady = false;
				}
			}
		}
		this.reconnected = true;
	}
	
	draw() {
		let realX = canvas.width * (this.xPos + 1) / 2 - 1, realY = canvas.height * (this.yPos + 1) / 2 - 1;
		ctx.fillRect(realX, realY, 3, 3);
		ctx.fillText(this.index, realX, realY);
		ctx.strokeStyle = "#F00";
		for (let r of this.angles) {
			ctx.beginPath();
			ctx.arc(realX, realY, 10, r[0] * 2 * Math.PI, r[1] * 2 * Math.PI);
			ctx.stroke();
		}
		ctx.strokeStyle = "rgba(0, 0, 0, 0.2)";
		ctx.beginPath();
		ctx.arc(realX, realY, canvas.width * tolerance / 2, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.strokeStyle = "rgba(0, 0, 0, 0.1)";
		ctx.beginPath();
		ctx.arc(realX, realY, canvas.width * conDist / 2, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.strokeStyle = "rgba(0, 0, 0, 0.05)";
		ctx.beginPath();
		ctx.arc(realX, realY, canvas.width * (conDist + tolerance) / 2, 0, 2 * Math.PI);
		ctx.stroke();
		ctx.strokeStyle = "rgba(0, 0, 0, 0.2)";
		ctx.beginPath();
		for (let c of Object.values(this.connections)) {
			ctx.moveTo(canvas.width * (this.xPos + 1) / 2, canvas.height * (this.yPos + 1) / 2);
			ctx.lineTo(canvas.width * (c.xPos + 1) / 2, canvas.height * (c.yPos + 1) / 2);
		}
		ctx.stroke();
	}
}

const canvas = document.getElementById("canvas");
canvas.width = canvas.height = 512;
const ctx = canvas.getContext('2d');

let players = Array(25).fill().map((x, i) => new Player(i));
for (let i = 1; i < players.length; i++) {
	players[i].connect(new PlayerConnection(
		players[i - 1].index,
		players[i - 1].xPos,
		players[i - 1].yPos,
		players[i - 1].angles,
		Object.keys(players[i - 1].connections)
	));
	players[i - 1].connect(new PlayerConnection(
		players[i].index,
		players[i].xPos,
		players[i].yPos,
		players[i].angles,
		Object.keys(players[i].connections)
	));
}

function loop() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	for (let p of players) p.move();
	for (let p of players) p.draw();
}

const timer = setInterval(loop, 25);
