class Player {
	constructor() {
		this.xPos = Math.random() * 2 - 1;
		this.yPos = Math.random() * 2 - 1;
		
		this.xVel = 0;
		this.yVel = 0;
		
		this.xAcc = 0;
		this.yAcc = 0;
		
		this.connections = [ ];
	}
	
	connect(player, connected) {
		this.connections.push(player);
		
		if (!connected) player.connect(this, true);
	}
	
	disconnect(player, connected) {
		this.connections.splice(this.connections.indexOf(player), 1);
		
		if (!connected) player.disconnect(this, true);
	}
	
	move() {
		this.xAcc += (Math.random() * 2 - 1) / 100;
		this.yAcc += (Math.random() * 2 - 1) / 100;
		
		if (this.xVel > 0.7) this.xAcc -= (this.xVel - 0.7) * 0.1;
		else if (this.xVel < -0.7) this.xAcc -= (this.xVel + 0.7) * 0.1;
		if (this.yVel > 0.7) this.yAcc -= (this.yVel - 0.7) * 0.1;
		else if (this.yVel < -0.7) this.yAcc -= (this.yVel + 0.7) * 0.1;
		
		this.xVel += this.xAcc / 100;
		this.yVel += this.yAcc / 100;
		
		if (this.xPos > 0.7) this.xVel -= (this.xPos - 0.7) * 0.1;
		else if (this.xPos < -0.7) this.xVel -= (this.xPos + 0.7) * 0.1;
		if (this.yPos > 0.7) this.yVel -= (this.yPos - 0.7) * 0.1;
		else if (this.yPos < -0.7) this.yVel -= (this.yPos + 0.7) * 0.1;
		
		this.xPos += this.xVel / 100;
		this.yPos += this.yVel / 100;
	}
	
	reconnect() {
		let angles = [];
		for (let c of this.connections) {
			let a = Math.atan2(c.yPos - this.yPos, c.xPos - this.xPos) / (2 * Math.PI) + 1;
			let start = a - 0.25;
			if (start > 1) start -= 1;
			let end = a + 0.25;
			if (end > 1) end -= 1;
			let current = 0;
			for (let i = 0; i <= angles.length; i++) {
				
			}
		}
		
		for (let c of this.connections) {
			for (let cc of c.connections) {
				if (cc == this
					|| this.connections.indexOf(cc) >= 0
					|| (cc.xPos - this.xPos) ** 2 + (cc.yPos - this.yPos) ** 2 > 0.2 ** 2) continue;
				
				this.connect(cc);
			}
			
			//if ((c.xPos - this.xPos) ** 2 + (c.yPos - this.yPos) ** 2 > 0.25 ** 2)
			//	this.disconnect(c);
		}
	}
	
	draw() {
		ctx.fillRect(canvas.width * (this.xPos + 1) / 2 - 1, canvas.height * (this.yPos + 1) / 2 - 1, 3, 3);
		ctx.beginPath();
		for (let c of this.connections) {
			ctx.moveTo(canvas.width * (this.xPos + 1) / 2, canvas.height * (this.yPos + 1) / 2);
			ctx.lineTo(canvas.width * (c.xPos + 1) / 2, canvas.height * (c.yPos + 1) / 2);
		}
		ctx.stroke();
	}
}

const canvas = document.getElementById("canvas");
canvas.width = canvas.height = 512;
const ctx = canvas.getContext('2d');

let players = Array(25).fill().map(x => new Player());
for (let i = 1; i < players.length; i++) players[i].connect(players[i - 1]);

function loop() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	for (let p of players) p.move();
	for (let p of players) p.reconnect();
	for (let p of players) p.draw();
}

const timer = setInterval(loop, 100);
